// ACTIVITY
let users = 
    [
    {
        "username": 'johndoe', 
        "password": 'johndoe123'
    },
    {
        "username": "ivana",
        "password": "143445254"
    }
    ]

/*
    gitbash > npm init 
    gitbash > npm install express
    .gitignore > node_modules


    1. Create a GET route that will access the "/home" route
        1.1 It should print the message: 'Welcome to the home page'
        1.2 Check the results on Postman
        1.3 Screenshot the result
        1.4 Make lagay the screenshot in a1 folder

    2. Create a GET route that will access the "/users"
        2.1 It should print the users 
        2.2 Check the result on Postman
        2.3 Screenshot the result
        2.4 Make lagay the screenshot in a1 folder

    // STRETCH GOAL
    3. Create a delete route that will access the "/delete-user" route
        3.1 It should print "The user has been deleted"
        3.2 Check the result on Postman
        3.3 Screenshot the result
        3.4 Make lagay the screenshot in a1 folder
*/
const express = require("express")
const app = express()
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post('/register', (request,response) => {
    if (request.body.username !== 0 && request.body.password !== 0){
        users.push(request.body);
        response.send(`User ${request.body.username} has been registered successfully`);
        console.log(request.body);
    } else {
        response.send('Please input BOTH username and password')
    }
})

app.get('/home', (request, response) => {
    response.send("Welcome to the home page")
})

app.get('/users', (request, response) => {
    response.send(users)
})

app.delete('/delete-user', (request, response) => {
    response.send(`User ${request.body.username} has been deleted`)
})
app.listen(port, () => console.log(`Server running at ${port}`))